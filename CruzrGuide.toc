\contentsline {section}{\numberline {1}Introduction}{3}%
\contentsline {section}{\numberline {2}Development}{4}%
\contentsline {subsection}{\numberline {2.1}Setting up Android Studio}{4}%
\contentsline {subsection}{\numberline {2.2}Setting up ADB (Android Debugging Bridge) for Cruzr}{4}%
\contentsline {subsection}{\numberline {2.3}Connecting to Cruzr via ADB}{5}%
\contentsline {subsection}{\numberline {2.4}Installing APK packages}{6}%
\contentsline {subsection}{\numberline {2.5}Programming Cruzr}{7}%
\contentsline {subsection}{\numberline {2.6}Ports in back panel}{7}%
\contentsline {subsection}{\numberline {2.7}Sensors}{8}%
\contentsline {section}{\numberline {3}Practical}{9}%
\contentsline {subsection}{\numberline {3.1}Accessing administrator mode}{9}%
\contentsline {subsection}{\numberline {3.2}Making maps and adding locations}{9}%
\contentsline {subsection}{\numberline {3.3}Adding a user using a picture file}{15}%
\contentsline {subsection}{\numberline {3.4}Cruzr APP}{16}%
